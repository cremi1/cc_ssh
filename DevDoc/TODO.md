# TOTO LISTE EN VRACS


## 1. `sshd_config`

1. Banner :  Communique si poste réservé : Astuce : [PreLogin Banner](https://www.cyberciti.biz/tips/change-openssh-sshd-server-login-banner.html)
2. Test limitation de l'accès

Pas d'exclusion, la clé ne fonctionne pas on peu saisir le MDP mais rien ne se passe!

Trouvé : autorisé les usagers listés ou les groupes  admin, root

```sshd_config
## CC_SSH_ID:UE_RESPONSABLE0_RESPONSABLE1
banner /etc/ssh/cc_ssh_banner
AllowUsers  aguermou
Match Group root
  AllowUsers *
Match Group admin
  AllowUsers *

```

## 2. vérification du json  : les conflits et la syntaxe

1. parse file / json import : attention aux dates
2. vérifier si c'est le bon dossier, le bon jour
3. vérifier les droits sur le dossier du jour
4. vérifier les hostname et uid!
5. vérifier si conflits ou pas!
6. +Tard : un éditeur!

## 3. `json` et `sshd_config`

- Autogénérer un `sshd_config` en fonction du json si besoin
- Contrôler au *boot* et via *cron* si il y a une session **CC_SSH** ou non :
  - remettre `sshd_config` par défaut
  - mettre `sshd_config` en mode **CC_SSH**


## vs-code : mes astuces 

### Créer un fichier `launch.json`
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python : Fichier actuel",
            "type": "python",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal",
            "args": [
                "-f",
                "C:\\Users\\Luigi\\Documents\\CREMI\\gitub\\cc_ssh\\EXAMPLE_UETEST_01.json"
            ]
        }
    ]
}
```



## Mes astuces Bash
```bash
for SALLE in  001 004 005 007 008 009 101 102 103 104 105 201 202 203 204 205 206 207 208 A21A A21B A21C; do
 COUNT=0
 COMMA=""
 printf "\"$SALLE\": {\n\"HOSTS\": {\n"
  for i in $(eval echo \$SALLE_$SALLE) ; do 
    printf $COMMA"\"%s\":[\"%s\"]\n" $i IDNUM_$COUNT; COUNT=$(($COUNT + 1))
    COMMA="," 
  done
 printf "  }\n},\n\n"
done

```


### Transformer un md to pdf :

```bash
pandoc    -V geometry:margin=2cm  -V papersize:a4 -V fontsize=12pt -o README.pdf README.md

```
