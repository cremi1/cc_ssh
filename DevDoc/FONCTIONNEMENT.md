# TP NOTE / Controle Continue en télétravail

## Principes dans les grandes lignes

  1. Réserver **un poste** pour *un usager* ou *plusieurs usagers* pour une durée limitéé
  2. Une fichier par réservation
     1. Un dossier central,
     2. un sous dossier par jour,
     3. un fichier par réservation
  3. Prévoir de garder l'accès aux admins et aux reponsables de la réservation
  4. Seul les enseignants peuvent réserver
  5. Prévoir une consultation des réservation
  6. Bien gérer le début fin de réservation
     1. Début : Tous les processus des usagers illégitimes  sont tuer.
     2. Fin : débloquer le poste

## Communication

Astuce : [PreLogin Banner](https://www.cyberciti.biz/tips/change-openssh-sshd-server-login-banner.html)


## Technique

### Coté Linux Client

Modifer `sshd_config`  :

- AllowUsers : le/les usagers et les reponsables!
- AllowGroup : toujours autoriser root et admin!

### Fichier json de réservation : détails des attributs

- date : toutes au format ISO8601  : `YYYY-MM-DD`T`HH:MM:SS`

- Le fichier json : `votresession.json` 

```json
{
  "SESSION": {
    "UE": "UETEST001",
    "DESCRIPTION": "Examen de TEST mode SSH",
    "DATE": {
       "START": "2020-04-22T9:20:00",
       "END":   "2020-04-22T12:40:00"
        },
    "RESPONSABLES": [ "cdelmon", "aguermou" ],
    "HOSTS": {
      "host0": [ "Login01", "Login02" ],
      "host1": [ "Login11", "Login12" ],
      "host2": [ "Login21", "Login22" ],
      "host3": [ "Login31", "Login32" ]
    }
  }
}

```

### Dossier de réservation : Controle continue

- Un sous dossier par jour.
- Un fichier par session, même si plusieurs salles sont utilisées.
- Vérifier le fichier avant, il sera déposer dans le bon dossier!

```text
/net/ens/CC_SSH
├── 20200413
│   └── RESEAU_UEXXX_01.json
├── 20200415
│   └── MATLAB_UZ ZZZ_01.json
└── 20200416
    ├── AR_FW_01.json
    └── AR_FW_02.json
```

### Pyton
