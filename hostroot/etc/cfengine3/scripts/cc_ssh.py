#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
  cc_ssh.py
    Utilisé par le cron pour mettre le poste en mode reservé cc_ssh ou non
    Fais parti du module cc_ssh :
    Permetre de faire des TPs Notés en télétravail en limitant l'usage
    d'un poste à certain usagers. 

    cc_ssh.py
     Usage
     -c, --cron : mode cron
     -n : DryRun : vérifie, affiche la conf sshd si besoin
     -f 

"""
# Built-in/Generic Imports
import os
import sys
import getopt
import psutil

# Own modules
from lib_cc_ssh import *

__author__ = 'Christeoph Delmon'
__copyright__ = 'Copyright 2020, cc_ssh'
__credits__ = ['Christophe Delmon', 'Abdou Guermouche']
__license__ = 'LGPL'
__version__ = '0.0.A'
__maintainer__ = 'Christophe Delmon'
__email__ = 'christophe.delmon@u-bordeaux.fr'


def usage():
  localfile=os.path.basename(__file__)
  print("%s : Usage" % localfile)
  print("\t         [-c, --cron] Modifie la conf et relance sshd si besoin")
  print("\t      [-n, --dry-run] Ne fais rien, explique juste ce qu'il y a à faire")
  print("\t        [-d, --debug] Active mode debug")
  print("\t          [-h,--help] Affiche ce message.")
  print("\t[-r path,--root=path] rootpath, pratique pour les debug. default /net/ens/CC_SSH")
  print("\t[-f file,--file=file] N'utilise que ce fichier ")
  print("\t        [-l,--reload] Force la creation du dichier sshd_config et recharge le service,")
  print("\t                      Fait aussi le ménage des processus des autres usagers")
  print("\t          [-s,--stop] Force l'arrêt de la réservation et relance sshd")
  

def main(): 
    """
    main()
        Fonction principale
    """
    try:
        opts, args = getopt.getopt(sys.argv[1:], "cndhlsf:r:", ["cron","dry-run","debug","help","reload","stop","file=","root="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err) # will print something like "option -x not recognized"
        usage()
        sys.exit(2)

    optfile = False
    basepath=False
    optcron = False
    optdryrun = False
    optreload = False
    optstop = False
    debug=False
    for o, a in opts:
        if o in ("-c", "--cron"):
            optcron = True
        elif o in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif o in ("-f", "--file"):
            optfile = a
        elif o in ("-r", "--root"):
            basepath = a
        elif o in ("-d", "--debug"):
            debug = True
        elif o in ("-l", "--reload"):
            optreload = True
        elif o in ("-n", "--dry-run"):
            optdryrun = True
        elif o in ("-s", "--stop"):
            optstop =True
        else:
            usage()
            assert False, "unhandled option"
    #Eviter de faire des modif par erreur!
    if not (optfile or optcron or optdryrun or optreload):
      optdryrun=True
  
    if optstop:
      sshd_config_restore_default()
      sshd_restart()
      sys.exit(0)
    if not basepath:
      data = get_session_now()
    else:
      data = get_session_now(basepath)

    # Pas de session
    if data == False:
      cc_ssh_active=if_cc_ssh_active(False) > 0
      if debug or optdryrun:
        print("Pas de fichier de session pour maintenant")
        if cc_ssh_active:
          print("Reste une trace de configuration d'ancienne session")
      # Une session est active mais ne doit pas y être
      if cc_ssh_active and (optcron or optreload) and not optdryrun:
        sshd_config_restore_default()
        sshd_restart()
    # Une session est en cours ou doit l'être!
    else:
      clean_local_session(data)
      cc_ssh_active=if_cc_ssh_active(data)
      if debug or optdryrun:
        print("Les données simplifiées : ")
        print(data)
        print("\nDEBUG: sshd_config\n")
        print(gen_sshd_config(data))
        print("\nDEBUG: sshd banner\n")
        print(gen_sshd_banner(data))
        print("\nDEBUG: Process Clean\n")
        kill_unwanted_proc(data,True)
        #Pour tester
        # sshd_restart()
        print("Session Type : %d" % cc_ssh_active)
        print("Banner exist : %b" % if_cc_ssh_banner_exist())
      if (optcron or optreload) and not optdryrun:
        # Si pas de session active ou pas la bonne relance sshd
        # le reload force aussi un kill des usagers non légitimes!
        if optreload or cc_ssh_active != 1 or not if_cc_ssh_banner_exist():
          sshd_config_rewrite(data)
          sshd_restart()
          kill_unwanted_proc(data)


if __name__ == "__main__":
    main()
    sys.exit(0)

  