#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
  cc_ssh_check.py
    Utilisé pour vérifier son fichier de réservation et si il n'y as pas de conflits.
    Fais parti du module cc_ssh :
    Permetre de faire des TPs Notés en télétravail en limitant l'usage
    d'un poste à certain usagers. 

 cc_ssh_check.py : Usage
        Si aucun argument teste le jour courant cc_ssh_check.py
        [-f, --file=] nomdefichier : teste le fichier si pas d'erreur.
        [-a, --all]   testes toutes les fichiers. **EN CONSTRUCTION**
        [-d, --debug] Active mode debug
        [-h,--help]   affiche ce message.
        [-r,--root=]  root path base, pratique pour les debug. default /net/ens/CC_SSH
        [-s,--save,]  Sauvegarde une version corrigé sav.json
        [-p,--place,]  Sauvegarde une version verrifié est corrigé dans le bon dossier
"""

# For debuging : launch.json
# "args": ["-h"],

# Built-in/Generic Imports
import os
import sys
import getopt

# Own modules
from lib_cc_ssh import *
# […]

__author__ = 'Christeoph Delmon'
__copyright__ = 'Copyright 2020, cc_ssh'
__credits__ = ['Christophe Delmon', 'Abdou Guermouche']
__license__ = 'LGPL'
__version__ = '0.0.B'
__maintainer__ = 'Christophe Delmon'
__email__ = 'christophe.delmon@u-bordeaux.fr'




def usage():
  """
   Usage()
    Fonction d'aide : seulement -h ou --help
  """
  localfile=os.path.basename(__file__)
  print("%s : Usage" % localfile)
  print("\tSi aucun argument teste le jour courant %s" % localfile)
  print("\t[-f, --file=] nomdefichier : teste le fichier si pas d'erreur.")
  print("\t[-a, --all]   testes toutes les fichiers. **EN CONSTRUCTION**")
  print("\t[-d, --debug] Active mode debug")
  print("\t[-h,--help]   affiche ce message.")
  print("\t[-r,--root=]  root path base, pratique pour les debug. default /net/ens/CC_SSH")
  print("\t[-s,--save,]  Sauvegarde une version corrigé sav.json")
  print("\t[-p,--place,]  Sauvegarde une version verrifié est corrigé dans le bon dossier")

def main(): 
    """
    main()
        Fonction principale
    """
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ahdspf:r:", ["all","help","debug","save","place","file=","root="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err) # will print something like "option -x not recognized"
        usage()
        sys.exit(2)
    optfile = False
    optsave = False
    optplace = False
    optall = False
    basepath=False
    debug=False
    for o, a in opts:
        if o in ("-a", "--all"):
            optall = True
        elif o in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif o in ("-f", "--file"):
            optfile = a
        elif o in ("-r", "--root"):
            basepath = a
        elif o in ("-d", "--debug"):
            debug = True
        elif o in ("-s", "--save"):
            optsave = True
        elif o in ("-p", "--place"):
            optplace = True
        else:
            assert False, "unhandled option"

    if len(opts) == 0  and len(args) == 1:
        optfile = args[0]

    if optfile != False:
        data = read_session_file(optfile)
        if data != False:
            if debug:
                print("Les données brutes : ")
                print(data)
            check_session(data)
            data["filename"] = optfile
            if debug:
                print("Toutes Les données nettoyées : ")
                print(data)
                print("DEBUG: ")
                if if_local_session(data):
                    print("\nDEBUG: sshd_config\n")
                    print(gen_sshd_config(data))
                    print("\nDEBUG: sshd banner\n")
                    print(gen_sshd_banner(data))
                else:
                    print("DEBUG \n Pas dans le bon créneau\n")
            print("Tous les postes et les usagers OK:")
            print(data["HOSTS"])
            print("Tous les reponsables OK:")
            print(data["RESPONSABLES"])
    elif not optall:
        if debug:
                print("On cherche le fichier du Jour")
        if not basepath:
            data = get_session_now()
        else:
            data = get_session_now(basepath)

        if data == False:
            if debug:
                print("Pas de fichier de session pour maintenant")
        else:    
            if debug:
                print("Les données brutes : ")
                print(data)
            check_session(data)
            if debug:
                print("Les données nettoyées : ")
                print(data)
                print("\nDEBUG: sshd_config\n")
                print(gen_sshd_config(data))
                print("\nDEBUG: sshd banner\n")
                print(gen_sshd_banner(data))
            print("Tous les postes et les usagers OK:")
            print(data["HOSTS"])
            print("Tous les reponsables OK:")
            print(data["RESPONSABLES"])
#Enregistrer en local ou en place la session vérifiée
    if (data != False):
        if optplace:
            if not basepath:
                saveandplace_session(data)
            else:
                saveandplace_session(data,basepath)
        elif optsave:
            save_session(data)
                
# Lancer le main program
if __name__ == "__main__":
    main()
    sys.exit(0)

  



