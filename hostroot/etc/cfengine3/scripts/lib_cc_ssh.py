#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
  lib-cc_ssh.py
   Bibliothèque de gestion des fichiers de réservation cc_ssh.
   Fais parti du module cc_ssh :
   Permetre de faire des TPs Notés en télétravail en limitant l'usage
   d'un poste à certain usagers. 

"""

# Built-in/Generic Imports
import os
import sys
import json
import socket
import platform
import dateutil.parser
import re
from pathlib import Path
from datetime import *

# Linux/Windows
if sys.platform.startswith("win"):
  import win32net
elif sys.platform.startswith("linux"):
  import pwd
  import grp


__author__ = 'Christeoph Delmon'
__copyright__ = 'Copyright 2020, cc_ssh'
__credits__ = ['Christophe Delmon', 'Abdou Guermouche']
__license__ = 'LGPL'
__version__ = '0.0.B'
__maintainer__ = 'Christophe Delmon'
__email__ = 'christophe.delmon@u-bordeaux.fr'

cc_ssh_root = "/net/ens/CC_SSH"
sshd_banner_file = "/tmp/cc_ssh"
sshd_config = "/etc/ssh/sshd_config"
min_uidnumber = 2000
stamp_head= "# #CC_SSH_ID# "
admin_grname= "admin"
  
def get_sessiondate(session):
  """recupère les datetimes des dates de départ et de fin de la session
   Paramètres :
   ------------
    session : le dict de la session
   Retour:
   -------
    Un dict des 2 datestime start et end  {"start" : datetime, "end": datetime}
  """
  stardate = dateutil.parser.parse(session["DATE"]["START"])
  enddate = dateutil.parser.parse(session["DATE"]["END"])
  return {"start": stardate, "end": enddate}

def get_today_session_path(basepath=cc_ssh_root):
  ldtime=datetime.now()
  daystamp=ldtime.strftime("%Y%m%d")
  return basepath + os.path.sep + daystamp

def get_session_now(basepath=cc_ssh_root):
  """
    Renvoie le dict représentant la session active,
    sinon retourne False
    :basepath: Optionnel : la base de recherche des fichier de session.

    NB: Fonctionnalite simplifiée : si on trouve un fichier session valide,
    on retourne la session et rien de plus!
  """
  daypath=get_today_session_path(basepath)
  ldtime=datetime.now()
  if os.path.exists(daypath):
    for sessionfile in os.listdir(daypath):
      data = read_session_file(daypath + os.path.sep +sessionfile)
      if if_local_session(data):
        data["filename"] = daypath + os.path.sep +sessionfile
        return data
  return False

def getlocalhost():
  """
   Renvoi le nom local du poste
  """
  return platform.node()

def check_hostname(hostname):
  """
   Renvoie True si le nom du poste existe
  """
  try:
    socket.getaddrinfo(hostname,22)
    return True
  except socket.error:
    return False

def clean_session_allhosts(session):
  """
    Teste si tous les nom de poste existent
    sinon les enlèves du dict
    :session: dict d'une session
  """
  check=True
  rlist=[]
  for h,u in session["HOSTS"].items():
    if not check_hostname(h):
        check=False
        print("Poste %s introuvable!" % h)
        rlist.append(h)
  for h in rlist:
    del session["HOSTS"][h]
  return check

def check_user(username):
  """
   Vérifie Si l'UID/username existe!
   :username: Nom de l'usager
  """
  if sys.platform.startswith("win"):
    try:
      win32net.NetUserGetInfo(None, username, 1).get("home_dir")
      return True
    except :
      return False
  elif sys.platform.startswith("linux"):
    try:
     pwd.getpwnam(username)
     return True
    except :
     return False
  return False

def clean_session_allusers(session):
  """
    Teste si tous les usagers existent
    sinon les enlèves de la liste des usagers
    :session: dict d'une session
  """
  check=True
  rdict={}
  for h,u in session["HOSTS"].items():
    rempty=True
    for user in u:
      if not check_user(user):
        check=False
        print("Utilisateur %s introuvable!" % user)
        if rempty:
          rempty=False
          rdict[h]=[]
        rdict[h].append(user)
  for h,u in rdict.items():
    for user in u:
     session["HOSTS"][h].remove(user)
  return check

def clean_session_responsables(session):
  """
    Teste si tous les responsables existent
    sinon les enlèves de la liste des reponsables
    :session: dict d'une session
  """
  rlist=[]
  check=True
  for user in session["RESPONSABLES"]:
    if not check_user(user):
      check=False
      print("Responsable %s introuvable!" % user)
      rlist.append(user)
  for user in rlist:
    session["RESPONSABLES"].remove(user)
  return check
  

def check_session(session):
  """
    Vérifie Une session
    1. Les hosts
    2. Les usagers
  """
  check=clean_session_allhosts(session)
  check=clean_session_allusers(session) and check
  check=clean_session_responsables(session) and check
  return check

def clean_local_session(session):
  """
    Garde que les élements pour la session locale
    :session: la session locale

    Ajoute dans la session "uids" la listes des uidnumber des usagers
    légitimes : utilisateurs + responsables + admins

  """
  localhost=getlocalhost()
  lhost=session["HOSTS"][localhost]
  session["HOSTS"]={}
  session["HOSTS"][localhost]=lhost
  check=clean_session_responsables(session)
  check=clean_session_allusers(session) and check
  if check and sys.platform.startswith("linux"):
    luids=[]
    auids=[]
    for user in session["HOSTS"][localhost] + list(set(session["RESPONSABLES"]) - set(session["HOSTS"][localhost])):
        luids.append(pwd.getpwnam(user).pw_uid)
    for user in grp.getgrnam(admin_grname).gr_mem:
        auids.append(pwd.getpwnam(user).pw_uid)
    session["uids"]=luids + list(set(auids) - set(luids)) 
  return check 

def read_session_file(filename):
  """
    Lit un fichier de description de session
    :filename: Nom du fichier au format json
    :return: dict si pas d'erreur sinon False
    NB, ajoute le "stamp" et les dates au format "datetime"
  Json File Format : 
  {
    "SESSION": {
      "UE": "UETEST001",
      "DESCRIPTION": "Examen de TEST mode SSH",
      "DATE": {
        "START": "2020-04-22T9:20:00A",
        "END": "2020-04-22T12:40:00A"
      },
      "RESPONSABLES": [
        "cdelmon", "aguermou"
      ],
      "HOSTS": {
        "host0": [ "Login01", "Login02"],
        "host1": [ "Login11", "Login12"],
        "host2": [ "Login21", "Login22"],
        "host3": ["Login31",  "Login32"]
      }
    }
  }
  """
  try:
    with open(filename, "r") as read_file:
      data = json.load(read_file)
  except IOError:
    print("Le fichier %s est introuvable" % filename)
    return False
  except ValueError:
    print("Erreur de decode json")
    return False
  if "SESSION" in data:
    session=data['SESSION']
    session["datetime"]=get_sessiondate(session)
    session["stamp"]=gen_session_stamp(session)
    return session
  else: 
    return False


def gen_session_stamp(session):
  """
    Renvoie la chaine de charactère représentant le "tampon"
    de la session!
  """
  responsables=""
  for r in session["RESPONSABLES"]:
    responsables+="_" + r
  return   stamp_head + " [" + session["UE"] + responsables + "]"


def gen_sshd_config(session):
  """
    Génère la fin du fichier sshd_config pour le poste local
    Les tests sont faits avants!
    Paramètre :
    -----------
     session : dict de la session
  """
  sstamp=session["stamp"]
  usersallow="AllowUsers"
  localhost=getlocalhost()
  sshd_banner="banner " + sshd_banner_file
  for user in session["HOSTS"][localhost] + list(set(session["RESPONSABLES"]) - set(session["HOSTS"][localhost])):
     usersallow+=" "+ user
  groupallow="\nMatch Group root\n  AllowUsers *\nMatch Group " + admin_grname + "\n  AllowUsers *\n"   
  debugmsg="\n# DEBUG pid " + str(os.getpid()) + "\n"
  debugmsg+="# Origine : " + session["filename"] + "\n"
  return sstamp + "\n" + sshd_banner +"\n" + usersallow + "\n" + groupallow + debugmsg


def gen_sshd_banner(session):
  """
    Renvoie le texte pour la bannière SSH
    Paramètre :
    -----------
     session : dict de la session
  """
  localhost=getlocalhost()
  buf= "#############################################\n"
  buf+="####  CE POSTE EST RESERVER POUR UN CC en SSH\n"
  buf+="#############################################\n"
  buf+="## UE           : " + session["UE"] + "\n"
  buf+="## Reponsables  : " + ' '.join(session["RESPONSABLES"]) + "\n"
  buf+="## Utilisateurs : " + ' '.join(session["HOSTS"][localhost]) + "\n"
  buf+="#############################################\n"
  buf+="#### Description  :                        ##\n"
  buf+="#### " + session["DESCRIPTION"]+ "\n"
  buf+="#############################################\n"
  buf+="#### Origine : " + session["filename"]+ "\n"
  return buf
  
def if_local_session(session):
  """
    Test si session active :
    1. L'heure est dans la plage horraire
    2. Si le poste y est bien
    Par principe le fichier de session à été vérifier avant!
    :session: dict de la session  
  """
  #L'horraire est OK
  if session["datetime"]["start"] <= datetime.now() <= session["datetime"]["end"]:
    #Le poste est dans la liste
    localhost=getlocalhost()
    if localhost in session["HOSTS"].keys():
     return True
  
  return False

### Toutes les fonction de sauvegarde

def save_session_sub(session):
 """
   Sous fonction commune qui enregistre
 """
 filename=session["filename"]
 del session["filename"]
 del session["datetime"]
 del session["stamp"]
 data={}
 data["SESSION"]=session
 try:
    with open(filename, "w") as outfile:
      json.dump(data,outfile)
    print("Fichier %s enregistré" % filename)
 except IOError:
    print("Le fichier %s est introuvable" % filename)
    return False
 except ValueError:
    print("Erreur de decode json")
    return False
 return True 
 

def saveandplace_session(session,basepath=cc_ssh_root):
  """
    Enregistre le fichier session, préalablement vérifié.
    Garde le même base name mais le place sur le bon répertoire
  """
  sessiondaystamp=session["datetime"]["start"].strftime("%Y%m%d")
  lpath=basepath + os.path.sep + sessiondaystamp
  bname=os.path.basename(session["filename"])
  newname=lpath + os.path.sep + bname
  if not os.path.exists(lpath):
    os.makedirs(lpath)
    os.chmod(lpath,0o2775)
  session["filename"]=newname
  return save_session_sub(session)


def save_session(session):
  """Enregistre le fichier session, préalablement vérifié.
    ajoute ".sav" au non du fichier
  """
  newfilename=session["filename"].replace(".json","") + ".sav.json"
  session["filename"]=newfilename
  return save_session_sub(session)




####################################################################################
####  Fonction Systèmes Linux
####################################################################################
#### Rappel : dans le dict(session) attributs sont ajoutés :
#### :uids: Listes des uid des usagers légitimes utilisateurs et responsables
#### :datetime: un dict avec le "datetime" de "start" et de "end"
#### :stamp: le stamp de la session
####
#### var : stamp_head
#### 
####  Ne pas tuer les processus appartenant aux uidnumber < min_uidnumber (2000)
####  Ne pas tuer les processus légitime la liste : session["uids"] 
#### 
#### 

import psutil

def if_cc_ssh_active(session=False):
  """Vérifie si cette session n'est pas active
   Paramètres :
   ------------
    session : dict de la session
    session["stamp"] est le stamp propre de cette session
   Variables :
   -----------
    stamp_head : l'entête des session ID
   Retour :
   ---------  
        0 : acune session active
        1 : session locale
      255 : Une autre session active 
  """
  sshd_config_contents=Path(sshd_config).read_text()
  if session != False:
    if sshd_config_contents.find(session["stamp"]) > 0:
      return 1
    elif sshd_config_contents.find(stamp_head) > 0:
      return 255
    else:
      return 0
  else:
    if sshd_config_contents.find(stamp_head) > 0:
      return 255
    else:
      return 0
    
def if_cc_ssh_banner_exist():
  """Teste si le fichier cc_ssh banner existe
    Retour :
    ---------
      True : Si le fichier existe
  """
  file = Path(sshd_banner_file)
  return file.exists()
    
def sshd_restart():
  """systemctl restart sshd
  """
  try:
    os.system("systemctl restart sshd.service")
  except e:
    print("Erreur lors de restart de sshd : %s" % e)


def sshd_config_rewrite(session=False):
  """Re-écris sshd_config
    parametres
    ----------
    session: dict  de la session
       si session=False remets un sshd_config par défaut
       si c'est un dict de session, ajoute la conf spéciale à la fin de sshd_config et crée le ssh_banner ad-hoc
  """
  #Restore si besoin et ont ajoute la configuraton
  sshd_config_contents=Path(sshd_config).read_text()
  stamp_index=sshd_config_contents.find(stamp_head)
  if stamp_index > 0:
    old_sshd_config_contents=sshd_config_contents[:stamp_index]
    sshd_config_contents=old_sshd_config_contents
  if session != False:
   sshd_config_contents+=gen_sshd_config(session)
   Path(sshd_banner_file).write_text(gen_sshd_banner(session))
  Path(sshd_config).write_text(sshd_config_contents)


def sshd_config_restore_default():
  """Remets un sshd_config sans les conf dédies pour cc_ssh
  """
  sshd_config_rewrite()


def kill_unwanted_proc(session,dryrun=False):
  """Tuer tous les processus des autres usagers
    parametres
    ----------
     session: dict 
      celui de la session,
      dryrun : bool
        *True* : on tue, *False* : Simple affichage
  """
  lproc=[]
  for proc in psutil.process_iter():
      uid=proc.uids().real
      if uid > min_uidnumber and uid not in session["uids"]:
        if dryrun:
          print(proc.as_dict(attrs=["username","pid","name"] ))
        else:
         try:
           proc.kill()
         except:
          #???
          print("Soucis de kill")






####################################################################################
#########  T O D O                                                           #######
####################################################################################
# ###
# Check de validité, aucun check de conflits, pas le temps :-(
# 
# Penser a faire un script en sudo pour le start et le stop
# Début de session nettoyage des jobs des autres usagers
#
#
#


  