# CC_SSH : contrôle continue en ssh

- Réserver pour des usagers des postes pour un usage privé via ssh.
- L'usager, les reponsables et les administrateurs seront les seul à accéder au poste en ssh.
- Tous les processus des usagers illégitimes seront tués au début de la session!
- Un message, banière ssh, sera affiché à la connexion sur le poste avant la saisie de Mot de passe 

## Préparation :

Cela peut ce faire depuis tous les postes de travail du CREMI!

### 1. Créer un fichier json de réservation :

#### Format du Fichier :
```json
{
  "SESSION": {
    "UE": "UETEST001",
    "DESCRIPTION": "Examen de TEST mode SSH",
    "DATE": {
       "START": "2020-04-22T9:20:00",
       "END":   "2020-04-22T12:40:00"
        },
    "RESPONSABLES": [ "cdelmon", "aguermou" ],
    "HOSTS": {
      "host0": [ "Login01", "Login02" ],
      "host1": [ "Login11", "Login12" ],
      "host2": [ "Login21", "Login22" ],
      "host3": [ "Login31", "Login32" ]
    }
  }
}

```

#### Contruction de ce fichier 
- pour aider le fichier suivant contient tous les postes de toutes les salles : 
```
/net/ens/CC_SSH/salles.json
```
- **Important** les dates sont au format **ISO8601**
- vs-code et son plugin "json editor" peuvent aider.
- Pas besoin de rajouter les reponsables sur chaque postes, c'est automatique

####  Pour trouver les idnum des usagers 
 - la commande Linux : `getent`
 - L'interface web de [gestion de comptes de CREMI](https://services.emi.u-bordeaux.fr/comptes)
     - recherche multiple possible sur mail ou N° Etu : bouton __[ajouter filtre]__ et bouton __[OU]__
     - recherche avancée : filtre ldap : exemple mixte email et N° Etu
```ldapfilter
(&(objectClass=ubxPerson)(|(supannEtuId=001)
(supannEtuId=002)
(supannEtuId=003)
(mail=adresse1@etu.u-bordeaux.fr)
(mail=adresse2@etu.u-bordeaux.fr)
(mail=adresse3@etu.u-bordeaux.fr)))
```

### 2. Le vérifier 

```bash
/etc/cfengine3/scripts/cc_ssh_check.py -f monfichier.json
```

### 3. Résever

```bash
/etc/cfengine3/scripts/cc_ssh_check.py -f monfichier.json -p
```

## Le jour j :


### a. Cron

Normalement tout ce fait via cron. rien à faire. Si des hosts sont down
il est toujours possible de faire un ajout dans votre fichier de session et de refaire la réservation!
`/etc/cfengine3/scripts/cc_ssh_check.py -f monfichier.json -p`

### b. Manuel
Doit être executer depuis le poste lui même, pour aider la commande `mssh`

#### 1. Activer et vérifier
```bash
sudo /etc/cfengine3/scripts/cc_ssh.py -c -d
```

#### 2. Désactiver
```bash
sudo /etc/cfengine3/scripts/cc_ssh.py -s
```
Ne pas oublier de supprimer ou déplacer le fichier de session qui est ici : 
```
/net/ens/CC_SSH/YYYYMMDD
```

#### 3. Forcer un reload de sshd / Nettoyer le processus

La commande suivante permet de recharger la conf de sshd et de tuer tous les processus non voulus.
Ne perturbe pas l'usager légitime qui aurait commencé sa session.
```bash
sudo /etc/cfengine3/scripts/cc_ssh.py -l
#ou
sudo /etc/cfengine3/scripts/cc_ssh.py --reload

```



